# hq

**H**ub-**Q**uality&trade; patrol.

This package simply installs [flake8](https://flakehell.readthedocs.io/config.html), [flakehell](https://flakehell.readthedocs.io/) and desired flake8 extensions.
In the future, a command line tool will be provided


## Usage

1. Configure [flakehell](https://flakehell.readthedocs.io/config.html). You can start from the provided [template/pyproject.toml](template/pyproject.toml).
2. Check your code by
```
    flakehell lint
```
